import numpy
import redis
from redis.commands.search.field import VectorField, TextField
from redis.commands.search.query import Query
import uuid
import sys

#connect to DB
def connectDB():
    r = redis.Redis(
        host = 'localhost',
        port = '3306',
        password ='admin'
    )
    r.ping()
    return r

def save_vectors(vector, url) : 
    r = connectDB()
    value ={}
    key = str(uuid.uuid4())
    p = r.pipeline(transaction=False)
    value['url'] = url
    value['vector'] =vector.astype(numpy.float32).tobytes()
    p.hset(key, mapping = value)
    p.execute()

def create_flat_index() :
    connectDB().ft().create_index([
        TextField("url"),
        VectorField("vector","FLAT",{
            "TYPE" : "FLOAT32",
            "DIM" : "256", #so chieu vector
            "DISTANCE_METRIC" : "L2", #su dung khoang cach euclid so sanh tuong dong
            "INITIAL_CAP" :"100", #So luong vector can luu tru
            "BLOCK_SIZE" : "100"
        })
    ])
    print("Indext created")
create_flat_index()
sys.sdtout.flush()    