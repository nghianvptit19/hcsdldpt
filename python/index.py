import cv2
from imageio import save
from skimage.feature import local_binary_pattern
import sys
import matplotlib.pyplot as plt
from skimage.transform import rotate
from skimage.feature import local_binary_pattern
from skimage import data
from skimage.color import label2rgb
import numpy as np
import matplotlib.pyplot as plt
#import save_vector

radius = 3
n_points = 8 * radius
def overlay_labels(image, lbp, labels):
    mask = np.logical_or.reduce([lbp == each for each in labels])
    return label2rgb(mask, image=image, bg_label=0, alpha=0.5)
def hist(ax, lbp):
    n_bins = int(lbp.max() + 1)
    return ax.hist(lbp.ravel(), density=True, bins=n_bins, range=(0, n_bins),
                   facecolor='0.5')
def do_LBP(path,url) :
    img = cv2.imread(path)
    img = cv2.resize(img,(256,256))
    image = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    lbp_image = local_binary_pattern(image,8,1,method = 'default')
    hist, _ = np.histogram(lbp_image.ravel(),density= True,bins = 256, range=(0,255))
    print(hist)
    fig, (ax_img, ax_hist) = plt.subplots(nrows=2, ncols=3, figsize=(9, 6))
    plt.gray()
    w = width = radius - 1
    edge_labels = range(n_points // 2 - w, n_points // 2 + w + 1)
    flat_labels = list(range(0, w + 1)) + list(range(n_points - w, n_points + 2))
    i_14 = n_points // 4            # 1/4th of the histogram
    i_34 = 3 * (n_points // 4)      # 3/4th of the histogram
    corner_labels = (list(range(i_14 - w, i_14 + w + 1)) +
                    list(range(i_34 - w, i_34 + w + 1)))

    label_sets = (edge_labels, flat_labels, corner_labels)

    for ax, labels in zip(ax_img, label_sets):
        ax.imshow(overlay_labels(image, lbp, labels))
    plt.show()
# plot histograms of LBP of textures

    #print(image[0])
    #print(image.shape)
    #print(lbp_image[0])
    #print(lbp_image.shape)
  #  save_vector.save_vectors(hist,url)
    return True


def get_vector(path) : 
    res = []
    img = cv2.imread(path)
    img = cv2.resize(img,(256,256))
do_LBP("D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\hcsdldpt\python\data\img1.jpg"," ")