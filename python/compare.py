import cv2
import numpy as np
from imageio import save
import numpy
import euclid
import math
from scipy.spatial import distance
from skimage.feature import local_binary_pattern

def compare(hist1,hist2,method) :
    return cv2.compareHist(hist1,hist2,method)


def hist(img_path) : 
    img = cv2.imread(img_path)
    img = cv2.resize(img,(256,256))
    image = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    lbp_image = local_binary_pattern(image,8,1,method = 'default')
    hist, _ = np.histogram(lbp_image.ravel(),density= True,bins = 256, range=(0,255))
    return hist

# method = [cv2.HISTCMP_CORREL,cv2.HISTCMP _CHISQR,cv2.HISTCMP _CHISQR_ALT,cv2.HISTCMP _INTERSECT
#           ,v2.HISTCMP _BHATTACHARYYA,cv2.HISTCMP _HELLINGER,"cv2.HISTCMP _KL_DIV"]

res1 = hist("D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\hcsdldpt\python\data\img1.jpg")
res2 = hist("D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\hcsdldpt\python\data\img1.jpg")
print(math.dist(res1,res2))
print(distance.euclidean(res1,res2))