# Importing all necessary libraries
import cv2
import os
import numpy as np
# Read the video from specified path
def extract_frames(path,i) :
    cam = cv2.VideoCapture(path)
    name_folder = './dataFrames/video'+str(i)
    try:
        # creating a folder named data
        if not os.path.exists(name_folder):
            os.makedirs(name_folder)
    # if not created then raise error
    except OSError:
        print ('Error: Creating directory of data')
    # frame
    currentframe = 0
    fps = cam.get(cv2.CAP_PROP_FPS)*2
    ret,frame = cam.read()


    hist1 = get_histogram_by_img(frame)

    file1 = open("./dataFrames/MyFile.txt", "w")
    file1.write(str(hist1.astype(np.float32).tobytes())+'\n')


    name = './dataFrames/video'+str(i)+'/frame' + str(currentframe) + '.jpg'
    print ('Creating...' + name)
    # writing the extracted images
    cv2.imwrite(name, frame)
    # increasing counter so that it will
    # show how many frames are created
    currentframe += 1

    while(True):
        # reading from frame
        ret,frame = cam.read()
        
        if ret :
            hist2 = get_histogram_by_img(frame)
            if compare_hist(hist1,hist2) < 0.8  :
                # if video is still left continue creating images
                name = './dataFrames/video'+str(i)+'/frame' + str(currentframe) + '.jpg'
                print ('Creating...' + name)
                # writing the extracted images
                cv2.imwrite(name, frame)
                # increasing counter so that it will
                # show how many frames are created
                hist1 = get_histogram_by_img(frame)

                file1 = open("./dataFrames/MyFile.txt", "w")
                file1.write(str(hist1.astype(np.float32).tobytes())+'\n')

            currentframe += 1
        else:
            break
    
    # Release all space and windows once done
    cam.release()
    cv2.destroyAllWindows()
    return True

# def with_opencv(filename):
#     import cv2
#     video = cv2.VideoCapture(filename)

#     fps = video.get(cv2.CAP_PROP_FPS)
#     frame_count = video.get(cv2.CAP_PROP_FRAME_COUNT)
#     duration = frame_count/fps
#     return duration, frame_count





def get_histogram_by_path(path) :
    img = cv2.imread(path) 
    img = cv2.resize(img,(256,256))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = []
    for i in range(3) :
        hist.append(cv2.calcHist([img],[i],None,[8],[0,256]))
        hist[i] = cv2.normalize(hist[i],hist[i]).flatten()
    return np.array(hist).flatten()
#Chi-square-compare
def compare_hist(hist1,hist2) :
    return cv2.compareHist(hist1,hist2,0)
    
def get_histogram_by_img(img) : 
    img = cv2.resize(img,(256,256))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    hist = []
    for i in range(3) :
        hist.append(cv2.calcHist([img],[i],None,[8],[0,256]))
        hist[i] = cv2.normalize(hist[i],hist[i]).flatten()
    return np.array(hist).flatten()



#extract key frames by Correlation
for i in range(0,22,21) :
    s = 'D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\hcsdldpt\clipped\movie' + str(i)+'.mp4'
    extract_frames(s,i)
#s = 'D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\hcsdldpt\clipped\movie0.mp4'












#print(len(get_histogram('D:\D19PTIT\D19_Ky2_Nam4\hcsdldpt\dataFrames\saa.jpg')))

#print(with_opencv(".\data\movie0.mp4"))
#extract_frames(".\data\movie0.mp4")